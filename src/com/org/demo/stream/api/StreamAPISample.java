package com.org.demo.stream.api;

import java.util.stream.IntStream;

public class StreamAPISample {
	
	public static void main(String[] args) {
		// Print 1-10 Integers
		IntStream
			.range(1, 10)
			.forEach(System.out::println);
	}

}
